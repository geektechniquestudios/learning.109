package com.geektechnique.practice.controllers;

import com.geektechnique.practice.commands.OnionForm;
import com.geektechnique.practice.converters.OnionToOnionForm;
import com.geektechnique.practice.domain.Onion;
import com.geektechnique.practice.services.OnionService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class OnionController {

    private OnionService onionService;
    private OnionToOnionForm onionToOnionForm;

    @Autowired
    public OnionController(OnionService onionService, OnionToOnionForm onionToOnionForm) {
        this.onionService = onionService;
        this.onionToOnionForm = onionToOnionForm;
    }

//    @RequestMapping("/") //commented out because conflict with security
//    public String redirToList(){
//        return "redirect:/onion/list";
//    }

    @GetMapping({"/onion/list", "/onion"})
    @ApiOperation("shows the onions the user has added in a list")
    public String listOnions(Model model){
        model.addAttribute("onions", onionService.listAllOnions());
        return "onion/list";
    }

    @GetMapping("/onion/show/{onionId}")
    @ApiOperation("shows specific onion based on id, called from thymeleaf when new onion is added")
    public String getOnion(@PathVariable String onionId, Model model){
        model.addAttribute("onion", onionService.getOnionById(onionId));
        return "onion/show";
    }

    @GetMapping("/onion/edit/{onionId}")
    @ApiOperation("enumerates form with an onion to edit from list")
    public String edit(@PathVariable String onionId, Model model){
        Onion onion = onionService.getOnionById(onionId);
        OnionForm onionForm = onionToOnionForm.convert(onion);

        model.addAttribute("onionForm", onionForm);
        return "onion/onionform";
    }

    @GetMapping("/onion/new")
    @ApiOperation("shows form for adding new onions")
    public String newOnion(Model model){
        model.addAttribute("onionForm", new OnionForm());
        return "onion/onionform";
    }

    @PostMapping("/onion")
    @ApiOperation("used for saving and updating onions")
    public String saveOrUpdateOnion(@Valid OnionForm onionForm, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "onion/onionform";
        }

        Onion savedOnion = onionService.saveOrUpdateOnionForm(onionForm);
        return "redirect:/onion/show/" + savedOnion.getOnionId();
    }

    @RequestMapping("/onion/delete/{onionId}")
    @ApiOperation("deletes a specific onion based on id")
    public String delete(@PathVariable String onionId){
        onionService.deleteOnion(onionId);
        return "redirect:/onion/list";
    }
}
