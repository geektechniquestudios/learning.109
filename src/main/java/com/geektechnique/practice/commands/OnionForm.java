package com.geektechnique.practice.commands;

import lombok.Data;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
public class OnionForm {

    @Id//not sure if I should use this here, or in the actual onion object class
    private String onionId;

    @NotNull //not really necc. bc min size is 2
    @Size(min = 2, max = 10)
    private String onionType;

    @NotNull
    @Digits(integer=5, fraction=2)// this means <=5000
//    @Size(min = 1, max = 1000) //seems I can't use @size with bigdec
    private BigDecimal onionPrice;
}
