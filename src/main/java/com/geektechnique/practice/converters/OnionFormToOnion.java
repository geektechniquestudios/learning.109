package com.geektechnique.practice.converters;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.Converter;
import com.geektechnique.practice.commands.OnionForm;
import com.geektechnique.practice.domain.Onion;
import org.springframework.stereotype.Component;
import org.thymeleaf.util.StringUtils;

@Component
public class OnionFormToOnion implements Converter<OnionForm, Onion> {
    @Override
    public Onion convert(OnionForm onionForm) {
        Onion onion = new Onion();
        if(onionForm.getOnionId() != null && !StringUtils.isEmpty(onionForm.getOnionId())){
            onion.setOnionId(onionForm.getOnionId());
        }
        onion.setOnionPrice(onionForm.getOnionPrice());
        onion.setOnionType(onionForm.getOnionType());

        return onion;
    }

    @Override
    public JavaType getInputType(TypeFactory typeFactory) {
        return null;
    }

    @Override
    public JavaType getOutputType(TypeFactory typeFactory) {
        return null;
    }
}
