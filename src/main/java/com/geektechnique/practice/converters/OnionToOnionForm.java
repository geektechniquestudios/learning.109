package com.geektechnique.practice.converters;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.Converter;
import com.geektechnique.practice.commands.OnionForm;
import com.geektechnique.practice.domain.Onion;
import org.springframework.stereotype.Component;

@Component
public class OnionToOnionForm implements Converter<Onion, OnionForm> {
    @Override
    public OnionForm convert(Onion onion) {

        OnionForm onionForm = new OnionForm();
        onionForm.setOnionId(onion.getOnionId());
        onionForm.setOnionPrice(onion.getOnionPrice());
        onionForm.setOnionType(onion.getOnionType());

        return onionForm;
    }

    @Override
    public JavaType getInputType(TypeFactory typeFactory) {
        return null;
    }

    @Override
    public JavaType getOutputType(TypeFactory typeFactory) {
        return null;
    }
}
