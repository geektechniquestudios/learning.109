package com.geektechnique.practice.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import javax.validation.constraints.Min;
import java.math.BigDecimal;

@RedisHash
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Onion {

    @Id private String onionId; //should I use uuid?
    private String onionType;
    private BigDecimal onionPrice;
}
