package com.geektechnique.practice.repositories;

import com.geektechnique.practice.domain.Onion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//@Repository
public interface OnionRepository extends CrudRepository<Onion, String> {
}
