package com.geektechnique.practice.services;

import com.geektechnique.practice.commands.OnionForm;
import com.geektechnique.practice.domain.Onion;

import java.util.List;

public interface OnionService {

    List<Onion> listAllOnions();
    Onion getOnionById(String id);
    Onion saveOrUpdateOnion(Onion onion);
    void deleteOnion(String id);
    Onion saveOrUpdateOnionForm(OnionForm onionForm);
}
