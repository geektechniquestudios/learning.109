package com.geektechnique.practice.services;

import com.geektechnique.practice.commands.OnionForm;
import com.geektechnique.practice.converters.OnionFormToOnion;
import com.geektechnique.practice.domain.Onion;
import com.geektechnique.practice.repositories.OnionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OnionServiceImpl implements OnionService {

    private OnionRepository onionRepository;
    private OnionFormToOnion onionFormToOnion;

    @Autowired
    public OnionServiceImpl(OnionRepository onionRepository, OnionFormToOnion onionFormToOnion) {
        this.onionRepository = onionRepository;
        this.onionFormToOnion = onionFormToOnion;
    }

    @Override
    public List<Onion> listAllOnions() {
        List<Onion> onions = new ArrayList<>();
        onionRepository.findAll().forEach(onions::add);
        return onions;
    }

    @Override
    public Onion getOnionById(String id) {
        return onionRepository.findById(id).orElse(null);
    }

    @Override
    public Onion saveOrUpdateOnion(Onion onion) {
        onionRepository.save(onion);
        return onion;
    }

    @Override
    public void deleteOnion(String id) {
        onionRepository.deleteById(id);
    }

    @Override
    public Onion saveOrUpdateOnionForm(OnionForm onionForm) {

        Onion savedOnion = saveOrUpdateOnion(onionFormToOnion.convert(onionForm));
        System.out.println("the onion you saved is a " + savedOnion.getOnionType());

        return savedOnion;
    }
}
